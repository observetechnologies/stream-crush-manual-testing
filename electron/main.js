const { app, BrowserWindow } = require('electron');
function createWindow () {
  app.commandLine.appendSwitch('ignore-certificate-errors', 'true');
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      webSecurity: false,
      allowRunningInsecureContent: true,
    },
  })
  win.webContents.openDevTools();
  win.loadURL('http://localhost:8080')
}
app.on('ready', createWindow);
